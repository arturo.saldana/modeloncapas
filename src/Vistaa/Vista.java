
package Vistaa;

import Logica.Logica;
import java.util.Scanner;

public class Vista {

    public static void main(String[] args)
    {
        Scanner respuesta = new Scanner(System.in);
        Logica logic = new Logica ();
        String res;
        
        System.out.println("A continuación se mostrará cada pregunta");
        for (int i = 1; i <= 10; i++)
        {
            Integer a = 0;
            boolean insertCheck = true;
            System.out.println(logic.consultarPreguntas(i));
            System.out.println("Ingrese su respuesta (1 = verdadero, 2= falso)");
            res = respuesta.next();
            
            try {
                int resInt = Integer.parseInt(res);
                if(resInt==1 || resInt == 2) {
                    logic.consultarRespuestas(resInt,i);
                } else {
                    System.out.println("Ud. ha ingresado un número incorrecto");
                    insertCheck = false;
                }
            } catch (NumberFormatException e) {
                System.out.println("Ud. ha ingresado un caracter incorrecto");
                insertCheck = false;
            } finally {
                if(!insertCheck) i--;
            }
        }
        //RESULTADO DEL EXAMEN
        System.out.println("A continuación se mostrará su puntaje");
        System.out.println(logic.MostrarResultados());
    }
    
}
